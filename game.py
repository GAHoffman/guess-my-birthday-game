# Includes For loop

#Import randint from random
from random import randint

# ask name with prompt "Hi! What is your name?"
name = input("Hi! What is your name?")
# up to five times, computer tries to guess birthday
# start initial guess_number at 1
# Guess <number between 1924 and 2004>: <name> were you born in <m> / <year>
# prompts Y/N response

# Need to create variables for each item computer will guess

# Can use For loop "for guess_number in range(5):"
# Can even put all this in a function to call

for guess_number in range(5):
    month_number = randint(1, 12)
    year_number = randint(1924, 2004)
    # If guess correctly, print affirmation
    print("Guess", guess_number, " :", name, "were you born in ", month_number, "/", year_number, "?")
    # prompt a response - just create response
    # variable and computer response off that
    response = input("yes or no?")
    # conditional statement based Y/N response
    if response == "yes":
        print("I knew it!")
        break
    elif response == "no" and guess_number < 4:
        print("Drat! Lemme try again!")
    else:
        print("I have other things to do. Good bye.")
